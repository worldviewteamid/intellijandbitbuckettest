package com.wellsfargo.ws.address.addressservice.controller;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/addresses")
public class AddressController {

    @GetMapping(path= "/entry" )
    public void entry( ) throws Exception {
        System.out.println("Addresses Entry");
    }

    @PostMapping(path= "/add", consumes = "application/json")
    public Boolean addAddress( @RequestBody Address address) throws Exception {
        System.out.println("Address Street :" + address.getStreet());
        return true;
    }

    @GetMapping(path= "/sample", produces = "application/json")
    public Address sampleAddress() throws Exception {
        System.out.println("SampleAddress");
        return buildSampleAddress();
    }

    private Address buildSampleAddress(){
        Address address = new Address();
        address.setFirstname("Meakue");
        address.setLastname("Dulleus");
        address.setStreet("231 Main Street");
        address.setCity("Harvest");
        address.setState("Idaho");
        address.setZip("90210");
        return address;
    }
}